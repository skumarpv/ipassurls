console.log('Loading redir_post_token func');
var AWS = require("aws-sdk");
var shortid = require('shortid');
var crypto = require('crypto');
const secret = "91yOurR0ck!t";
const TABLE_NAME = "redir_urls";

// setup dynamodb configuration
var dynamoDBConfiguration = {
    "region": "us-east-1"
  };
AWS.config.update(dynamoDBConfiguration);
var docClient = new AWS.DynamoDB.DocumentClient();

/**
 * AWS lamda handler for POST
 */
exports.handler = function(event, context) {
    
    console.log("request received:\n", JSON.stringify(event));
    console.log("context received:\n", JSON.stringify(context));

    var shortDomain = (event.shortDomain === undefined ? "ipas.io" : event.shortDomain);
    var longUrl = (event.longUrl === undefined ? "NO URL" : event.longUrl);
    var hash = crypto.createHmac('sha256', secret)
                   .update(longUrl)
                   .digest('hex');

    handlePostRequest(shortDomain, longUrl, hash, context);

}

/**
 * Method handles a post request call and decides if the longUrl needs 
 * to be updated or if it has to be created.
 */
var handlePostRequest = function(shortDomain, longUrl, hash, context) {

    var params = {
        TableName: TABLE_NAME,
        Key: { 
            hashKey: hash
        }
    };
    
    // check if the longUrl exists and then decide the next action
    docClient.get(params, function(err, data) {
        
        // no error
        if(err) {
            console.log("error: getItem");
            //console.log(err);
            handleCreateRequest(shortDomain, longUrl, hash, context);
        }
        else {

            if(data) {
                // longUrl already exists
                if ("Item" in data && "shortUrl" in data.Item) {
                    console.log("longUrl already exists");
                    var shortUrl = data.Item.shortUrl;
                    var retValue = {
                        "shortUrl": shortUrl,
                        "message": "already exists"
                    };
                    context.succeed(retValue);

                } else {

                    handleCreateRequest(shortDomain, longUrl, hash, context);
                }
            }
            else {
                
                handleCreateRequest(shortDomain, longUrl, hash, context);
            }
        }   
        
    });
}

/**
 * Creates a new entry for a longUrl in the table: redir_urls
 */
var handleCreateRequest = function(shortDomain, longUrl, hash, context) {

    console.log("creating new entry: " + longUrl);
    var shortUrlId = shortid.generate();
    var shortUrl = shortDomain + "/" + shortUrlId;
    var item = {
        "hashKey": hash,
        "shortId": shortUrlId,        
        "shortDomain": shortDomain,
        "shortUrl": shortUrl,
        "longUrl": longUrl,
        "createdAt": new Date().toISOString()
    };

    var params = {
        TableName: TABLE_NAME,
        Item: item,
        ReturnValues:"ALL_OLD" // need to revisit this !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    };

    docClient.put(params, function(err, data) {
        if(err) {
            var retValue = {
                "status": "ERROR",
                "message": "D01"
            }
            context.fail(retValue);
        }
        else {
            var retValue = {
                "shortUrl": shortUrl,
                "status": "OK"
            };
        	context.succeed(retValue);
        }
    });
} 