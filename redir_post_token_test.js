console.log('Loading redir_post_token func');
var AWS = require("aws-sdk");
var shortid = require('shortid');
var crypto = require('crypto');
const secret = "91yOurR0ck!t";
const TABLE_NAME = "redir_urls";

// setup dynamodb configuration
var dynamoDBConfiguration = {
    "accessKeyId": "AKIAJNUEMDCRXXSZUXWQ",
    "secretAccessKey": "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",
    "region": "us-east-1"
  };
AWS.config.update(dynamoDBConfiguration);
var docClient = new AWS.DynamoDB.DocumentClient();


var testFunc = function(event, context, callback) {
    
    console.log("request received:\n", JSON.stringify(event));
    console.log("context received:\n", JSON.stringify(context));

    var shortDomain = (event.shortDomain === undefined ? "ipas.io" : event.shortDomain);
    var longUrl = (event.longUrl === undefined ? "NO URL" : event.longUrl);
    var hash = crypto.createHmac('sha256', secret)
                   .update(longUrl)
                   .digest('hex');

    handlePostRequest(shortDomain, longUrl, hash);
}

/**
 * Method handles a post request call and decides if the longUrl needs 
 * to be updated or if it has to be created.
 */
var handlePostRequest = function(shortDomain, longUrl, hash, callback) {

    var params = {
        TableName: TABLE_NAME,
        Key: { 
            hashKey: hash
        }
    };
    
    // check if the longUrl exists and then decide the next action
    docClient.get(params, function(err, data) {
        
        // no error
        if(err) {
            console.log("error: getItem");
            //console.log(err);
            handleCreateRequest(shortDomain, longUrl, hash);
        }
        else {

            if(data) {
                // longUrl already exists
                if ("Item" in data && "shortUrl" in data.Item) {
                    console.log("longUrl already exists");
                    var shortUrl = data.Item.shortUrl;
                    var retValue = {
                        "shortUrl": shortUrl,
                        "message": "already exists"
                    };
                    console.log(JSON.stringify(retValue));
                    //context.succeed(retValue);
                } else {
                    var content = "<html><body><h1>Not Found</h1></body></html>";
                    handleCreateRequest(shortDomain, longUrl, hash);
                }
            }
            else {
                handleCreateRequest(shortDomain, longUrl, hash);
            }
        }   
        
    });
}

/**
 * Creates a new entry for a longUrl in the table: redir_urls
 */
var handleCreateRequest = function(shortDomain, longUrl, hash, callback) {

    console.log("creating new entry: " + longUrl);
    var shortUrlId = shortid.generate();
    var shortUrl = shortDomain + "/" + shortUrlId;
    var item = {
        "hashKey": hash,
        "shortId": shortUrlId,        
        "shortDomain": shortDomain,
        "shortUrl": shortUrl,
        "longUrl": longUrl,
        "createdAt": new Date().toISOString()
    };

    var params = {
        TableName: TABLE_NAME,
        Item: item,
        ReturnValues:"ALL_OLD"
    };

    docClient.put(params, function(err, data) {
        if(err) {
            var retValue = {
                "status": "ERROR",
                "message": "D01"
            }
            context.fail(retValue);
        }
        else {
            var retValue = {
                "shortUrl": shortUrl,
                "status": "OK"
            };
        	context.succeed(retValue);
        }
    });
} 

var event = {
	"shortDomain": "https://8ttmfml54c.execute-api.us-east-1.amazonaws.com/test/shorten",
	"longUrl": "https://symphonyqa.ipass.com/euservices/activation/?companyId=1022878#/gettingStarted?params=dGhlemhhbmdkeW5hc3R5IaUPpEaJrK36kQVFWy_nf5Y2l3IjZF4CS9BN4OHvy-l9ySXNxE6PqBcoRyaO3k3-x2GlGVInT7Kqv7jNQRGOSx0R-2twgYRj7vOVjs6cLDPNEGg54ZdGI95CCOSw6ChhpTZEKuYqIqkH2m_dLUWWGzLvZFoeswp10bVtlnB0WgdCPVAG6imOCvrHNJoNt-asdasdasdsxx000a",
    "longUrl1": "https://symphonyqa.ipass.com/euservices/activation/?companyId=1022878#/gettingStarted?params=dGhlemhhbmdkeW5hc3R5IaUPpEaJrK36kQVFWy_nf5Y2l3IjZF4CS9BN4OHvy-l9ySXNxE6PqBcoRyaO3k3-x2GlGVInT7Kqv7jNQRGOSx0R-2twgYRj7vOVjs6cLDPNEGg54ZdGI95CCOSw6ChhpTZEKuYqIqkH2m_dLUWWGzLvZFoeswp10bVtlnB0WgdCPVAG6imOCvrHNJoNt-asdasdasdsxx0908"
};
//testFunc(event, null);
const hash1 = crypto.createHmac('sha256', secret)
                .update(event.longUrl)
                .digest('hex');
const hash2 = crypto.createHmac('sha256', secret)
                .update(event.longUrl1)
                .digest('hex');

testFunc(event);
//console.log(hash);
//console.log(hash2);
