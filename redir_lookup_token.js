console.log('Loading redir_lookup_token func');
var AWS = require("aws-sdk");
var dynamoDBConfiguration = {
    "region": "us-east-1"
  };
AWS.config.update(dynamoDBConfiguration);
var docClient = new AWS.DynamoDB();
const TABLE_NAME = "redir_urls";


/**
 * Main AWS lamda handler
 */
exports.handler = function(event, context) {
    
    console.log("request received:\n", JSON.stringify(event));
    //console.log("token received:\n", event.pathParams.token);
    //console.log("event received:\n", event);
    //console.log("Context received:\n", JSON.stringify(context));
    
    fetchLocationRedirect(event.pathParams.token, context)
}


/**
 * Function to fetch the redirect destination URL (longUrl)
 */
var fetchLocationRedirect = function(shortId, context) {

    var params = {
        TableName: TABLE_NAME,
        IndexName: "ShortIdIndex",
        KeyConditionExpression: "shortId = :s_shortId",
        ExpressionAttributeValues: {
            ":s_shortId": {"S": shortId}
        }
    };
    
    docClient.query(params, function(err, data) {
        if(err) {
            console.log("error: getItem - " + err);
            context.fail({"longUrl": "", "status": "ERROR"});
        }
            
         //console.log(JSON.stringify(data));
         if(data.Items.length == 1) {
             var longUrl = data.Items[0].longUrl.S;
             context.succeed({"longUrl": longUrl, "status": "OK"});
         }
         else {
             context.fail({"longUrl": "", "status": "NOT_FOUND"});
         }
    });
}
