# README #

POC for iPass URL shortening service.

### What is this repository for? ###

* This is iPass's own URL shortening service and is still in POC stage.
* Version 0.0

### How do I get set up? ###

* Get access to a AWS account with access rights to API Gateway, Lamda and DynamoDB services.
* Create an API in the AWS gateway for POST & GET methods. Some additional configuration is required for GET method which will be shared in a separate document
* Dependencies
* Create a table named 'redir_urls' in DynamoDB. Create a scondary index named 'ShortIdIndex' with attributes mapped - 'longUrl, shortUrl'.
* Create Lamda functions for POST & GET methods. The corresponding files in the repo are 'redir_post_token.js' and 'redir_lookup_token.js'.
* Deploy API gateway into any stage and run the commands.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Sunil